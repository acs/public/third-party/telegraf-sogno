package json_struct

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	//"log"
	//"strconv"
	"time"

	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/filter"
	//"github.com/influxdata/telegraf/internal"
	//"github.com/influxdata/telegraf/metric"
	"github.com/tidwall/gjson"
)

var (
	utf8BOM      = []byte("\xef\xbb\xbf")
	ErrWrongType = errors.New("must be an object or an array of objects")
)

type Config struct {
	MetricName   string
	TagKeys      []string
	NameKey      string
	StringFields []string
	Query        string
	TimeKey      string
	TimeFormat   string
	Timezone     string
	DefaultTags  map[string]string
	Strict       bool
	StructName	 string
}

type Parser struct {
	metricName   string
	tagKeys      []string
	stringFields filter.Filter
	nameKey      string
	query        string
	timeKey      string
	timeFormat   string
	timezone     string
	defaultTags  map[string]string
	strict       bool
	structName	 string
}

func New(config *Config) (*Parser, error) {
	stringFilter, err := filter.Compile(config.StringFields)
	if err != nil {
		return nil, err
	}

	return &Parser{
		metricName:   config.MetricName,
		tagKeys:      config.TagKeys,
		nameKey:      config.NameKey,
		stringFields: stringFilter,
		query:        config.Query,
		timeKey:      config.TimeKey,
		timeFormat:   config.TimeFormat,
		timezone:     config.Timezone,
		defaultTags:  config.DefaultTags,
		strict:       config.Strict,
		structName:	  config.StructName,
	}, nil
}

func (p *Parser) parseIntoStruct(data []byte) ([]telegraf.Metric, error) {

	name := p.metricName

	// set timestamp to current time
	nTime := time.Now().UTC()

	switch p.structName {
	case "loco-pmu":
		var m LocoPMUMessage
		err := json.Unmarshal(data, &m)
		if err != nil {
			return nil, err
		}
		results, err := m.getMetrics(name, nTime, p.timeFormat, p.timezone, p.defaultTags)
		return results, err
	case "mac-pmu":
		var m MacPMUMessage
		err := json.Unmarshal(data, &m)
		if err != nil {
			return nil, err
		}
		results, err := m.getMetrics(name, nTime, p.timeFormat, p.timezone, p.defaultTags)
		return results, err
	case "sogno-device":
		var m SognoDeviceMessage
		err := json.Unmarshal(data, &m)
		if err != nil {
			return nil, err
		}
		results, err := m.getMetrics(name, nTime, p.timeFormat, p.timezone, p.defaultTags)
		return results, err
	case "sogno-service":
		var m SognoServiceMessage
		err := json.Unmarshal(data, &m)
		if err != nil {
			return nil, err
		}
		results, err := m.getMetrics(name, nTime, p.timeFormat, p.timezone, p.defaultTags)
		return results, err
	}
	return nil, nil
}

// Parse takes byte array as input and
// returns array of metrics
func (p *Parser) Parse(buf []byte) ([]telegraf.Metric, error) {
	if p.query != "" {
		result := gjson.GetBytes(buf, p.query)
		buf = []byte(result.Raw)
	}

	buf = bytes.TrimSpace(buf)
	buf = bytes.TrimPrefix(buf, utf8BOM)
	if len(buf) == 0 {
		return make([]telegraf.Metric, 0), nil
	}

	return p.parseIntoStruct(buf)
}

// ParseLine do not use
func (p *Parser) ParseLine(line string) (telegraf.Metric, error) {
	metrics, err := p.Parse([]byte(line + "\n"))

	if err != nil {
		return nil, err
	}

	if len(metrics) < 1 {
		return nil, fmt.Errorf("can not parse the line: %s, for data format: json_struct", line)
	}

	return metrics[0], nil
}

// SetDefaultTags sets defaultTags to parameter tags
func (p *Parser) SetDefaultTags(tags map[string]string) {
	p.defaultTags = tags
}



