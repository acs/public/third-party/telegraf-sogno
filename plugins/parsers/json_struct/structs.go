package json_struct

import(
	"time"

	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/metric"
	"github.com/influxdata/telegraf/internal"
)

//LocoPMUMessage is the message format sent by the RWTH LOCO PMU
type LocoPMUMessage struct {
  Device string `json:"device"`
	MeasID string `json:"meas_id"`
	Timestamp string `json:"timestamp"`
	Type string `json:"type"`
	Data float64 `json:"data"`
}

func (m *LocoPMUMessage) getMetrics(name string, nTime time.Time, timeFormat string, timezone string, defaultTags map[string]string) ([]telegraf.Metric, error) {
	results := make([]telegraf.Metric, 0)

	tags := make(map[string]string)
	for k, v := range defaultTags {
		tags[k] = v
	}
	tags["device"] = m.Device
	tags["type"] = m.Type
	tags["meas_id"] = m.MeasID

	nFields := make(map[string]interface{})
	nFields["data"] = m.Data
	nFields["acq_timestamp"] = m.Timestamp

	metricItem, err := metric.New(name, tags, nFields, nTime)
	results = append(results, []telegraf.Metric{metricItem}...)
	if err != nil {
		return nil, err
	}

	return results, nil
}

// MacPMUMessageRecordings describes
// the readings field in MAC PMU messages
type MacPMUMessageRecordings struct {
  Source string `json:"source"`
	Type string `json:"type"`
	Data float64 `json:"data,string"`
}

// MacPMUMessage describes the JSON structure for MAC messages
type MacPMUMessage struct {
  Identifier string `json:"identifier"`
	Timestamp string `json:"timestamp"`
	Readings []MacPMUMessageRecordings `json:"readings"`
}

func (m *MacPMUMessage) getMetrics(name string, nTime time.Time, timeFormat string, timezone string, defaultTags map[string]string) ([]telegraf.Metric, error) {
	results := make([]telegraf.Metric, 0)

	for _, item := range m.Readings {

		tags := make(map[string]string)
		for k, v := range defaultTags {
			tags[k] = v
		}
		tags["identifier"] = m.Identifier
		tags["type"] = item.Type
		tags["source"] = item.Source

		nFields := make(map[string]interface{})
		nFields["data"] = item.Data
		nFields["acq_timestamp"] = m.Timestamp

		metricItem, err := metric.New(name, tags, nFields, nTime)
		results = append(results, []telegraf.Metric{metricItem}...)
		if err != nil {
			return nil, err
		}
	}

	return results, nil
}

// SognoDeviceMessageRecordings describes
// the readings field in SognoDeviceMessage
type SognoDeviceMessageRecordings struct {
	Component string `json:"component"`
	Measurand string `json:"measurand"`
	Phase string `json:"phase"`
	Data float64 `json:"data"`
}

// SognoDeviceMessage describes the JSON structure
// for SOGNO device messages
type SognoDeviceMessage struct {
	Device string `json:"device"`
	Timestamp string `json:"timestamp"`
	Readings []SognoDeviceMessageRecordings `json:"readings"`
}

func (m *SognoDeviceMessage) getMetrics(name string, nTime time.Time, timeFormat string, timezone string, defaultTags map[string]string) ([]telegraf.Metric, error) {
results := make([]telegraf.Metric, 0)

	if timeFormat == "" {
		timeFormat = time.RFC3339Nano
	}

	nTime, err := internal.ParseTimestamp(timeFormat, m.Timestamp, timezone)
	if err != nil {
		return nil, err
	}

	for _, item := range m.Readings {
		tags := make(map[string]string)
		for k, v := range defaultTags {
			tags[k] = v
		}
		tags["device"] = m.Device
		tags["component"] = item.Component
		tags["measurand"] = item.Measurand
		tags["phase"] = item.Phase

		nFields := make(map[string]interface{})
		nFields["data"] = item.Data

		metricItem, err := metric.New(name, tags, nFields, nTime)
		results = append(results, []telegraf.Metric{metricItem}...)
		if err != nil {
			return nil, err
		}
	}
	return results, nil
}

// SognoServiceMessageRecordings describes
// the readings field in SognoServiceMessage
type SognoServiceMessageRecordings struct {
	Measurand string `json:"measurand"`
	Phase string `json:"phase"`
	Data float64 `json:"data"`
}

// SognoServiceMessage describes the JSON structure
// for SOGNO device messages
type SognoServiceMessage struct {
	Component string `json:"component"`
	Timestamp string `json:"timestamp"`
	Readings []SognoServiceMessageRecordings `json:"readings"`
}

func (m *SognoServiceMessage) getMetrics(name string, nTime time.Time, timeFormat string, timezone string, defaultTags map[string]string) ([]telegraf.Metric, error) {
results := make([]telegraf.Metric, 0)

	if timeFormat == "" {
		timeFormat = time.RFC3339Nano
	}
	nTime, err := internal.ParseTimestamp(timeFormat, m.Timestamp, timezone)
	if err != nil {
		return nil, err
	}

	for _, item := range m.Readings {
		tags := make(map[string]string)
		for k, v := range defaultTags {
			tags[k] = v
		}
		tags["component"] = m.Component
		tags["measurand"] = item.Measurand
		tags["phase"] = item.Phase

		nFields := make(map[string]interface{})
		nFields["data"] = item.Data

		metricItem, err := metric.New(name, tags, nFields, nTime)
		results = append(results, []telegraf.Metric{metricItem}...)
		if err != nil {
			return nil, err
		}
	}
	return results, nil
}